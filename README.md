Este proyecto ejemplifica la integración de la aplicación BIRT Report Viewer (4.2.0) con una aplicación web existente.
Este proyecto se ha creado como una aplicación Java Web -> Web Application con NetBeans 8.1.

Para mayor información consulte: https://www.ibm.com/developerworks/library/ba-birt-viewer-java-webapps/
